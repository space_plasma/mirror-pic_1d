
add_executable(test-LibTPT EXCLUDE_FROM_ALL)
target_sources(test-LibTPT PRIVATE
    test-main.cc
    test-FieldGrid.cc
    test-Particle.cc
    test-ParticleTracer.cc
    )

target_link_libraries(test-LibTPT PRIVATE
    Catch2::Catch2 LibTPT
    )

set_project_warnings(test-LibTPT)
enable_sanitizers(test-LibTPT)

add_test("Test-LibTPT" test-LibTPT)
