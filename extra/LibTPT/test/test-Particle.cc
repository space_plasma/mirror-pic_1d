/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "catch2/catch.hpp"

#include <TPT/Particle.h>

#include <PIC/UTL/println.h>
#include <iostream>

namespace {
template <class T, class U>
[[nodiscard]] bool operator==(Detail::VectorTemplate<T, double> const &a, Detail::VectorTemplate<U, double> const &b) noexcept
{
    return a.x == Approx{ b.x }.margin(1e-15)
        && a.y == Approx{ b.y }.margin(1e-15)
        && a.z == Approx{ b.z }.margin(1e-15);
}
template <class T1, class T2, class U1, class U2>
[[nodiscard]] bool operator==(Detail::FourVectorTemplate<T1, T2> const &a, Detail::FourVectorTemplate<U1, U2> const &b) noexcept
{
    return a.t == Approx{ b.t }.margin(1e-15) && a.s == b.s;
}
[[nodiscard]] bool operator==(CurviCoord const &a, CurviCoord const &b) noexcept
{
    return a.q1 == b.q1;
}
} // namespace
using ::operator==;

TEST_CASE("Test Lib::Particle", "[Lib::Particle]")
{
    auto const Oc0 = -1.3994;
    auto const vel = CartVector{ 1 };
    auto const pos = CurviCoord{ 2 };

    auto const ptl = TPT::Particle{ Oc0, vel, pos };
    println(std::cout, "particle = ", ptl);

    CHECK(ptl.Oc0 == Oc0);
    CHECK(ptl.vel == vel);
    CHECK(ptl.pos == pos);
}

TEST_CASE("Test Lib::RelativisticParticle", "[Lib::RelativisticParticle]")
{
    auto const Oc0  = -1.3994;
    auto const gc   = Scalar{ 1.3 };
    auto const gvel = CartVector{ 1 };
    auto const pos  = CurviCoord{ 2 };

    auto const ptl = TPT::RelativisticParticle{ Oc0, { gc, gvel }, pos };
    println(std::cout, "rel_particle = ", ptl);

    CHECK(ptl.Oc0 == Oc0);
    CHECK(ptl.gcgvel == FourCartVector{ gc, gvel });
    CHECK(ptl.pos == pos);
}
