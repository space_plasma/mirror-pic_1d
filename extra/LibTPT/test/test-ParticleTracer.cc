/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "catch2/catch.hpp"

#include <TPT/ParticleTracer.h>

#include <PIC/UTL/println.h>
#include <cmath>
#include <fstream>
#include <sstream>

namespace {
constexpr bool dump_result = false;
}

TEST_CASE("Test LibTPT::ParticleTracer::Mirror", "[LibTPT::ParticleTracer::Mirror]")
{
    Real const c        = 8;
    Real const O0       = 1;
    Real const xi       = 0.05;
    Real const D1       = 0.10725;
    long const Nx       = 120 * 2;
    Real const dt       = 0.01;
    long const inner_Nt = 10;
    long const outer_Nt = 200;
    Real const Oc0      = 2 * M_PI;

    auto const geo    = Geometry{ xi, D1, O0 };
    auto const tracer = TPT::ParticleTracer{ c, Range{ 0, (inner_Nt * outer_Nt + 0.1) * dt }, Range{ -1, 2 } * Nx / 2.0, geo };

    {
        auto const pos      = CurviCoord{ 0 };
        auto const vel      = geo.mfa_to_cart(MFAVector{ 3.141592653589794, 5.441398092702653, 0. }, pos);
        auto const beta     = vel / c;
        auto const gamma    = 1 / std::sqrt(1 - dot(beta, beta));
        auto const particle = TPT::RelativisticParticle{ Oc0, lorentz_boost<-1>(FourCartVector{ c, {} }, beta, gamma), pos };

        std::ostringstream ss;
        ss.setf(ss.fixed);
        ss.precision(15);
        println(ss, '{');
        tracer.trace(0, particle, dt, [&ss](long i, Real t, auto const &ptl) -> bool {
            if (0 == i % inner_Nt) {
                if (i)
                    println(ss, ',');
                print(ss, "{\"i\" -> ", i, ", \"t\" -> ", t, ", \"q1\" -> ", ptl.pos.q1, ", \"gvel\" -> ", ptl.gcgvel.s, '}');
            }
            return true;
        });
        println(ss, "\n}");

        if (dump_result) {
            std::ofstream os{ "/Users/kyungguk/Downloads/dump1.m" };
            os << ss.str();
        }
    }

    {
        auto const pos      = CurviCoord{ 0 };
        auto const vel      = geo.mfa_to_cart(MFAVector{ 5.441398092702654, 3.1415926535897927, 0. }, pos);
        auto const beta     = vel / c;
        auto const gamma    = 1 / std::sqrt(1 - dot(beta, beta));
        auto const particle = TPT::RelativisticParticle{ Oc0, lorentz_boost<-1>(FourCartVector{ c, {} }, beta, gamma), pos };

        std::ostringstream ss;
        ss.setf(ss.fixed);
        ss.precision(15);
        println(ss, '{');
        tracer.trace(0, particle, dt, [&ss](long i, Real t, auto const &ptl) -> bool {
            if (0 == i % inner_Nt) {
                if (i)
                    println(ss, ',');
                print(ss, "{\"i\" -> ", i, ", \"t\" -> ", t, ", \"q1\" -> ", ptl.pos.q1, ", \"gvel\" -> ", ptl.gcgvel.s, '}');
            }
            return true;
        });
        println(ss, "\n}");

        if (dump_result) {
            std::ofstream os{ "/Users/kyungguk/Downloads/dump2.m" };
            os << ss.str();
        }
    }
}

TEST_CASE("Test LibTPT::ParticleTracer::HarmonicOscillator", "[LibTPT::ParticleTracer::HarmonicOscillator]")
{
    Real const c   = 0.5;
    Real const O0  = 1;
    Real const xi  = 0.0;
    long const Nx  = 120 * 2;
    Real const w0  = 2 * M_PI;
    long const Nt  = 360;
    Real const dt  = 2 * M_PI / w0 / Nt;
    Real const D1  = 1 / w0;
    Real const Oc0 = 1e10;
    auto const geo = Geometry{ xi, D1, O0 };

    std::vector<CartVector> dE;
    auto const              t_range = Range{ 0, Nt } * dt;
    auto const              q_range = Range{ -1, 2 };
    for (int t_idx = 0; t_idx <= Nt; ++t_idx) {
        for (int s_idx = 0; s_idx <= Nx; ++s_idx) {
            auto const curvi = CurviCoord{ s_idx * (q_range.len / Nx) + q_range.min() };
            auto const cart  = geo.cotrans(curvi);
            auto const F     = -w0 * w0 * cart.x;
            dE.push_back(CartVector{ F / (c * Oc0), 0, 0 });
        }
    }

    auto const tracer
        = TPT::ParticleTracer(c, t_range, q_range, geo, TPT::FieldGrid::make_unique({ t_range, q_range }),
                              TPT::FieldGrid::make_unique({ t_range, q_range }, { Nt + 1, Nx + 1 }, std::move(dE)));

    {
        auto const gcgvel = lorentz_boost<-1>(FourCartVector{ c, {} }, CartVector{ 1, 0, 0 } / c);
        auto const pos    = CurviCoord{};
        auto const ptl    = TPT::RelativisticParticle{ Oc0, gcgvel, pos + 0.5 * dt * CurviCoord{ geo.cart_to_contr(gcgvel.s * c / *gcgvel.t, pos) } };

        std::ostringstream ss;
        ss.setf(ss.fixed);
        ss.precision(15);
        println(ss, '{');
        tracer.trace(dt / 2, ptl, dt, [&ss](long i, Real t, auto const &ptl) -> bool {
            if (i)
                println(ss, ',');
            print(ss, "{\"i\" -> ", i, ", \"t\" -> ", t, ", \"q1\" -> ", ptl.pos.q1, ", \"gvel\" -> ", ptl.gcgvel.s, '}');

            return true;
        });
        println(ss, "\n}");

        if (dump_result) {
            std::ofstream os{ "/Users/kyungguk/Downloads/dump3.m" };
            os << ss.str();
        }
    }
}
