/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "catch2/catch.hpp"

#include <TPT/FieldGrid.h>

#include <PIC/UTL/println.h>

#include <iostream>
#include <random>

TEST_CASE("Test LibTPT::FieldGrid", "[LibTPT::FieldGrid]")
{
    auto const f = [](Real t, Real q) {
        return t + q;
    };

    long const t_dim    = 11;
    long const q_dim    = 13;
    auto const t_extent = Range{ -1, 2 };
    auto const q_extent = Range{ 1, 3 };

    std::vector<CartVector> z;
    for (int t_idx = 0; t_idx < t_dim; ++t_idx) {
        auto const t = t_idx * t_extent.len / (t_dim - 1) + t_extent.min();
        for (int q_idx = 0; q_idx < q_dim; ++q_idx) {
            auto const q = q_idx * q_extent.len / (q_dim - 1) + q_extent.min();
            z.emplace_back(f(t, q));
        }
    }

    auto const field1 = TPT::FieldGrid({ t_extent, q_extent }, { t_dim, q_dim }, std::move(z));
    // println(std::cout, field1);

    CHECK_NOTHROW(field1.interp(t_extent.min(), CurviCoord{ q_extent.min() }));
    CHECK_NOTHROW(field1.interp(t_extent.min(), CurviCoord{ q_extent.max() - 1e-10 }));
    CHECK_NOTHROW(field1.interp(t_extent.max() - 1e-10, CurviCoord{ q_extent.min() }));
    CHECK_NOTHROW(field1.interp(t_extent.max() - 1e-10, CurviCoord{ q_extent.max() - 1e-10 }));

    auto const field2 = TPT::FieldGrid({ t_extent, q_extent }, CartVector{ 1 });
    CHECK_NOTHROW(field2.interp(t_extent.min(), CurviCoord{ q_extent.min() }));
    CHECK_NOTHROW(field2.interp(t_extent.min(), CurviCoord{ q_extent.max() - 1e-10 }));
    CHECK_NOTHROW(field2.interp(t_extent.max() - 1e-10, CurviCoord{ q_extent.min() }));
    CHECK_NOTHROW(field2.interp(t_extent.max() - 1e-10, CurviCoord{ q_extent.max() - 1e-10 }));
    println(std::cout, field2);

    std::mt19937                     rng{ 4938 };
    std::uniform_real_distribution<> t_dist{ t_extent.min(), t_extent.max() };
    std::uniform_real_distribution<> q_dist{ q_extent.min(), q_extent.max() };
    for (long i = 0; i < 1000; ++i) {
        auto const t = t_dist(rng);
        auto const q = q_dist(rng);
        REQUIRE(field1.interp(t, CurviCoord{ q }).x == Approx{ f(t, q) }.margin(1e-10));
        REQUIRE(field2.interp(t, CurviCoord{ q }).x == Approx{ 1 }.epsilon(1e-10));
    }
}
