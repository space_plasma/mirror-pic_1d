/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <TPT/Config.h>

#include <PIC/CurviCoord.h>
#include <PIC/Shape.h>
#include <PIC/UTL/Range.h>
#include <PIC/VT/Vector.h>

#include <memory>
#include <ostream>
#include <utility>
#include <vector>

LIBTPT_NAMESPACE_BEGIN
class FieldGrid {
    static constexpr ShapeOrder order = ShapeOrder::_1st;
    static_assert(order >= 1);

public:
    FieldGrid(FieldGrid &&) noexcept = delete;
    FieldGrid &operator=(FieldGrid &&) noexcept = delete;

    struct Extents {
        Range time;
        Range space;
    };
    struct Indices {
        long time;
        long space;

        [[nodiscard]] friend constexpr auto operator+(Indices indices, long i) noexcept
        {
            indices.time += i;
            indices.space += i;
            return indices;
        }
    };
    using Backend = std::vector<CartVector>;

    Extents const               extents{};
    Indices const               dims{};
    Backend const               data{};
    std::pair<Real, Real> const deltas{};

    ~FieldGrid();
    FieldGrid(Extents const &, Indices const &dims, Backend);
    explicit FieldGrid(Extents const &extents, Backend::value_type const &fill_value = {}) // creates an object filled with fill values
    : FieldGrid{ extents, { 2, 2 }, FieldGrid::Backend(4, fill_value) }
    {
    }
    [[nodiscard]] static auto make_unique(Extents const &extents, Indices const &dims, Backend data)
    {
        return std::make_unique<FieldGrid>(extents, dims, std::move(data));
    }
    [[nodiscard]] static auto make_unique(Extents const &extents, Backend::value_type const &fill_value = {})
    {
        return std::make_unique<FieldGrid>(extents, fill_value);
    }

    [[nodiscard]] auto interp(Real time, CurviCoord const &) const -> CartVector;

private:
    [[nodiscard]] bool within(Indices const &idx) const noexcept
    {
        return (idx.time >= 0 && idx.time < dims.time)
            && (idx.space >= 0 && idx.space < dims.space);
    }
    [[nodiscard]] auto indices(long const &idx) const noexcept // linear index to 2D indices
    {
        return Indices{ idx / dims.space, idx % dims.space };
    }
    [[nodiscard]] auto index(Indices const &idx) const noexcept // 2D indices to linear index
    {
        auto const [time, space] = idx;
        return time * dims.space + space;
    }
    [[nodiscard]] auto at(Indices const &idx) const -> CartVector;

    template <long Order>
    [[nodiscard]] auto impl_interp(Shape<Order> const &time, Shape<Order> const &space) const -> CartVector;

    // pretty print (buffered)
    //
    template <class CharT, class Traits>
    friend decltype(auto) operator<<(std::basic_ostream<CharT, Traits> &os, Extents const &extents)
    {
        return os << '{' << extents.time << ", " << extents.space << '}';
    }
    template <class CharT, class Traits>
    friend decltype(auto) operator<<(std::basic_ostream<CharT, Traits> &os, Indices const &indices)
    {
        return os << '{' << indices.time << ", " << indices.space << '}';
    }
    template <class CharT, class Traits>
    friend decltype(auto) operator<<(std::basic_ostream<CharT, Traits> &os, FieldGrid const &grid)
    {
        os << "{\n";

        // attributes
        os << "\t\"dims\" -> " << grid.dims << ",\n";
        os << "\t\"extents\" -> " << grid.extents << ",\n";
        os << "\t\"data\" -> {\n";

        // first entry of data; guaranteed to be at least one element
        long idx = 0;
        os << "\t\t" << grid.indices(idx) + 1 << " -> " << grid.at(grid.indices(idx));
        ++idx;

        // the rest of data
        for (long const size = long(grid.data.size()); idx < size; ++idx) {
            os << ",\n\t\t" << grid.indices(idx) + 1 << " -> " << grid.at(grid.indices(idx));
        }
        os << "\n\t}";

        return os << "\n}";
    }
};
LIBTPT_NAMESPACE_END
