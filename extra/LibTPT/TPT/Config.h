/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

// specify version of dependent library
//
#define LIBPIC_INLINE_VERSION 1

#define LIBTPT_NAMESPACE TPT
#define LIBTPT_NAMESPACE_BEGIN \
    namespace LIBTPT_NAMESPACE {
#define LIBTPT_NAMESPACE_END \
    }
