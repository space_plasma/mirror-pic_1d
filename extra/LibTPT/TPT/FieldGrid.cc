/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "FieldGrid.h"

#include <stdexcept>
#include <string>
#include <utility>

LIBTPT_NAMESPACE_BEGIN
FieldGrid::~FieldGrid()
{
}
FieldGrid::FieldGrid(Extents const &extents, Indices const &dims, Backend _data)
: extents{ extents }
, dims{ dims }
, data{ std::move(_data) }
, deltas{ extents.time.len / Real(dims.time - 1), extents.space.len / Real(dims.space - 1) }
{
    // check time, space extents
    if (auto const [time, space] = extents; time.len <= 0)
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ } + " - invalid time extent" };
    else if (space.len <= 0)
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ } + " - invalid space extent" };

    // check time, space dimensions
    if (auto const [time, space] = dims; time <= order)
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ } + " - invalid time dimension" };
    else if (space <= order)
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ } + " - invalid space dimension" };
    else if (static_cast<long>(data.size()) != time * space)
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ } + " - data size and dimensions mismatch" };
}

auto FieldGrid::at(Indices const &idx) const -> CartVector
{
    if (!within(idx))
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ } + " - out of range: index = {" + std::to_string(idx.time) + ", " + std::to_string(idx.space) + "}" };
    else
        return *std::next(begin(data), index(idx));
}
auto FieldGrid::interp(Real const time, CurviCoord const &pos) const -> CartVector
{
    return impl_interp(Shape<order>{ (time - extents.time.min()) / deltas.first },
                       Shape<order>{ (pos.q1 - extents.space.min()) / deltas.second });
}
template <long Order>
auto FieldGrid::impl_interp(Shape<Order> const &time, Shape<Order> const &space) const -> CartVector
{
    auto const inner = [&space, this](long const time) {
        auto interp = Backend::value_type{};
        for (unsigned j = 0; j <= Order; ++j) {
            interp += at({ time, space.i(j) }) * space.w(j);
        }
        return interp;
    };
    auto const outer = [&time, inner] {
        auto interp = Backend::value_type{};
        for (unsigned j = 0; j <= Order; ++j) {
            interp += inner(time.i(j)) * time.w(j);
        }
        return interp;
    };
    return outer();
}
LIBTPT_NAMESPACE_END
