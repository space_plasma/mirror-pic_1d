/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <TPT/Config.h>
#include <TPT/FieldGrid.h>
#include <TPT/Particle.h>

#include <PIC/BorisPush.h>
#include <PIC/Geometry.h>

#include <functional>
#include <memory>
#include <ostream>
#include <type_traits>
#include <utility>

LIBTPT_NAMESPACE_BEGIN
class ParticleTracer {
    static constexpr BC const   boundary                   = BC::reflecting;
    static constexpr bool const should_randomize_gyrophase = true;

    std::shared_ptr<FieldGrid const> m_bfield;
    std::shared_ptr<FieldGrid const> m_efield;

public:
    ParticleTracer(ParticleTracer &&) noexcept = delete;
    ParticleTracer &operator=(ParticleTracer &&) noexcept = delete;

    Real const     c;
    Range const    time_extent;
    Range const    space_extent;
    Geometry const geomtr;

    [[nodiscard]] auto &bfield() const noexcept
    {
        return *m_bfield;
    }
    [[nodiscard]] auto &efield() const noexcept
    {
        return *m_efield;
    }

    ~ParticleTracer();
    ParticleTracer(Real c, Range time_extent, Range space_extent, Geometry, std::shared_ptr<FieldGrid const> bfield, std::shared_ptr<FieldGrid const> efield);
    ParticleTracer(Real c, Range time_extent, Range space_extent, Geometry geometry, std::weak_ptr<FieldGrid const> bfield, std::weak_ptr<FieldGrid const> efield)
    : ParticleTracer(c, time_extent, space_extent, geometry, bfield.lock(), efield.lock())
    {
    }
    ParticleTracer(Real c, Range time_extent, Range space_extent, Geometry geometry) // no external field
    : ParticleTracer(c, time_extent, space_extent, geometry,
                     FieldGrid::make_unique({ time_extent, space_extent }),
                     FieldGrid::make_unique({ time_extent, space_extent }))
    {
    }
    template <class... Args>
    [[nodiscard]] static auto make_unique(Args &&...args)
    {
        return std::make_unique<ParticleTracer>(std::forward<Args>(args)...);
    }

    template <class TestParticle, class StepMonitor>
    auto trace(Real const tinit, TestParticle particle, Real const dt, StepMonitor monitor) const -> std::enable_if_t<
        std::is_same_v<TestParticle, Particle> || std::is_same_v<TestParticle, RelativisticParticle>,
        std::pair<Real, TestParticle>>
    {
        static_assert(std::is_invocable_r_v<bool, StepMonitor, long, Real, TestParticle>);

        // particle position at full-time step x(n)
        // particle velocity at half-time step v(n-1/2)
        auto const boris = BorisPush{ dt, c, geomtr.B0(), particle.Oc0 };
        Real       t     = tinit;
        long       i     = 0;
        while (predicate(i, t, particle, monitor)) {
            particle.pos += single_step(boris, t, particle) * dt;
            boundary_pass(particle);
            //
            t = tinit + Real(++i) * dt;
        }

        return std::make_pair(t, particle);
    }

private:
    template <class TestParticle, class StepMonitor>
    [[nodiscard]] bool predicate(long const i, Real const t, TestParticle const &particle, StepMonitor const &monitor) const
    {
        return time_extent.is_member(t)
            && space_extent.is_member(particle.pos.q1)
            && std::invoke(monitor, i, t, particle);
    }

    [[nodiscard]] auto background_bfield(RelativisticParticle const &) const -> CartVector;
    [[nodiscard]] auto single_step(BorisPush const &, Real const &t0, RelativisticParticle &) const -> CurviCoord; // return moved / dt

    template <class TestParticle>
    void boundary_pass(TestParticle &particle) const
    {
        switch (boundary) {
            case BC::periodic:
                periodic_boundary(particle);
                break;
            case BC::reflecting:
                reflecting_boundary(particle);
                break;
        }
    }
    template <class TestParticle>
    void periodic_boundary(TestParticle &particle) const;
    void reflecting_boundary(RelativisticParticle &particle) const;

    [[nodiscard]] auto randomize_gyrophase(MFAVector const &) const -> MFAVector;

    template <class CharT, class Traits>
    friend decltype(auto) operator<<(std::basic_ostream<CharT, Traits> &os, ParticleTracer const &tracer)
    {
        os << "{\n";

        // attributes
        os << "\t\"c\" -> " << tracer.c << ",\n";
        os << "\t\"time_extent\" -> " << tracer.time_extent << ",\n";
        os << "\t\"space_extent\" -> " << tracer.space_extent << ",\n";
        os << "\t\"xi\" -> " << tracer.geomtr.xi() << ",\n";
        os << "\t\"D\" -> " << tracer.geomtr.D() << ",\n";
        os << "\t\"O0\" -> " << tracer.geomtr.B0() << ",\n";
        os << "\t\"bfield\" -> " << tracer.bfield() << ",\n";
        os << "\t\"efield\" -> " << tracer.efield() << "\n}";

        return os;
    }
};
LIBTPT_NAMESPACE_END
