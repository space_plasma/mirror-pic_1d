/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "ParticleTracer.h"

#include <cmath>
#include <random>
#include <utility>

LIBTPT_NAMESPACE_BEGIN
ParticleTracer::~ParticleTracer()
{
}
ParticleTracer::ParticleTracer(Real c, Range time_extent, Range space_extent, Geometry geomtr, std::shared_ptr<FieldGrid const> _bfield, std::shared_ptr<FieldGrid const> _efield)
: c{ c }
, time_extent{ time_extent }
, space_extent{ space_extent }
, geomtr{ geomtr }
, m_bfield{ std::move(_bfield) }
, m_efield{ std::move(_efield) }
{
    if (!m_bfield || !m_efield)
        throw std::invalid_argument{ __PRETTY_FUNCTION__ };
}

auto ParticleTracer::background_bfield(RelativisticParticle const &ptl) const -> CartVector
{
    // get gyro-radius offset: rL = e1 x γv / Oc (Oc is a signed "proper" gyro-freq)
    auto const Oc   = ptl.Oc0 * geomtr.Bmag_div_B0(ptl.pos);
    auto const gvel = ptl.gcgvel.s;
    auto const rL_y = -gvel.z / Oc;
    auto const rL_z = +gvel.y / Oc;

    return geomtr.Bcart(ptl.pos, rL_y, rL_z);
}
auto ParticleTracer::single_step(BorisPush const &boris, Real const &t0, RelativisticParticle &particle) const -> CurviCoord
{
    // field at full time step B(n) and E(n)
    auto const dE = efield().interp(t0, particle.pos);
    auto const dB = bfield().interp(t0, particle.pos);
    auto const B0 = background_bfield(particle);

    // advance velocity v(n-1/2) -> v(n+1/2)
    boris.relativistic(particle.gcgvel, dB + B0, dE);

    // advance position x(n) -> x(n+1)
    return CurviCoord{ geomtr.cart_to_contr(particle.velocity(c), particle.pos) };
}

template <class TestParticle>
void ParticleTracer::periodic_boundary(TestParticle &ptl) const
{
    if (auto const &pos = ptl.pos; pos.q1 < space_extent.min())
        ptl.pos.q1 += space_extent.len;
    else if (pos.q1 >= space_extent.max())
        ptl.pos.q1 -= space_extent.len;
}
void ParticleTracer::reflecting_boundary(RelativisticParticle &ptl) const
{
    if (auto const &pos = ptl.pos; pos.q1 < space_extent.min()) {
        // crossed left boundary; put back into the leftmost cell
        ptl.pos.q1 += 2 * (space_extent.min() - ptl.pos.q1);
        // flip the momentum component parallel to B0
        CartVector const e1 = geomtr.e1(ptl.pos);
        ptl.gcgvel.s -= 2 * dot(ptl.gcgvel.s, e1) * e1;
        // gyro-phase randomization
        if constexpr (should_randomize_gyrophase)
            ptl.gcgvel.s = geomtr.mfa_to_cart(randomize_gyrophase(geomtr.cart_to_mfa(ptl.gcgvel.s, ptl.pos)), ptl.pos);
    } else if (pos.q1 >= space_extent.max()) {
        // crossed right boundary; put back into the rightmost cell
        ptl.pos.q1 += 2 * (space_extent.max() - ptl.pos.q1);
        // flip the momentum component parallel to B0
        CartVector const e1 = geomtr.e1(ptl.pos);
        ptl.gcgvel.s -= 2 * dot(ptl.gcgvel.s, e1) * e1;
        // gyro-phase randomization
        if constexpr (should_randomize_gyrophase)
            ptl.gcgvel.s = geomtr.mfa_to_cart(randomize_gyrophase(geomtr.cart_to_mfa(ptl.gcgvel.s, ptl.pos)), ptl.pos);
    }
}
auto ParticleTracer::randomize_gyrophase(MFAVector const &v) const -> MFAVector
{
    thread_local static std::mt19937                     rng{ 594737 };
    thread_local static std::uniform_real_distribution<> dist{ -M_PI, M_PI };

    auto const phase = dist(rng);
    auto const cos   = std::cos(phase);
    auto const sin   = std::sin(phase);

    return { v.x, v.y * cos - v.z * sin, v.y * sin + v.z * cos };
}
LIBTPT_NAMESPACE_END
