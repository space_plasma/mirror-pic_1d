/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <TPT/Config.h>

#include <PIC/CurviCoord.h>
#include <PIC/VT/FourVector.h>
#include <PIC/VT/Vector.h>

#include <ostream>

LIBTPT_NAMESPACE_BEGIN
struct Particle final {
    using Vector = CartVector;
    using Real   = Vector::ElementType;

    Real       Oc0; //!< equatorial (signed) cyclotron frequency
    Vector     vel; //!< 3-component velocity vector
    CurviCoord pos; //!< curvilinear coordinates

    explicit Particle() noexcept = default;
    constexpr Particle(Real Oc0, Vector const &vel, CurviCoord const &pos) noexcept
    : Oc0{ Oc0 }, vel{ vel }, pos{ pos }
    {
    }

    // pretty print
    template <class CharT, class Traits>
    friend decltype(auto) operator<<(std::basic_ostream<CharT, Traits> &os, Particle const &ptl)
    {
        return os << "{\"Oc0\" -> " << ptl.Oc0 << ", \"vel\" -> " << ptl.vel << ", \"pos\" -> " << ptl.pos << '}';
    }
};

struct RelativisticParticle final {
    using FourVector = FourCartVector;
    using Real       = FourVector::ElementType;

    Real       Oc0;    //!< equatorial (signed) cyclotron frequency
    FourVector gcgvel; //!< γ*{c, v}
    CurviCoord pos;    //!< curvilinear coordinates

    [[nodiscard]] auto beta() const noexcept
    {
        return gcgvel.s / Real{ gcgvel.t };
    }
    [[nodiscard]] auto velocity(Real const c) const noexcept
    {
        return gcgvel.s * (c / *gcgvel.t); // usual velocity
    }

    explicit RelativisticParticle() noexcept = default;
    constexpr RelativisticParticle(Real Oc0, FourVector const &gcgvel, CurviCoord const &pos) noexcept
    : Oc0{ Oc0 }, gcgvel{ gcgvel }, pos{ pos }
    {
    }

    // pretty print
    template <class CharT, class Traits>
    friend decltype(auto) operator<<(std::basic_ostream<CharT, Traits> &os, RelativisticParticle const &ptl)
    {
        return os << "{\"Oc0\" -> " << ptl.Oc0 << ", \"gcgvel\" -> " << ptl.gcgvel << ", \"pos\" -> " << ptl.pos << '}';
    }
};
LIBTPT_NAMESPACE_END
