/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "FieldGridLink.h"
#include "MArgumentGetter.h"

#include <TPT/FieldGrid.h>

#include <algorithm>
#include <map>
#include <memory>

namespace {
std::map<mint, std::shared_ptr<TPT::FieldGrid const>> field_grid_pool{};
}
auto field_grid_at(WolframLibraryData libData, mint const id) -> std::weak_ptr<TPT::FieldGrid const>
{
    if (auto const it = field_grid_pool.find(id); it == field_grid_pool.end()) {
        log_error(__FUNCTION__, " - the slot for id = ", id, " does not exist");
        libData->Message("args");
        return {};
    } else if (auto const &field_grid = it->second; !field_grid) {
        log_error(__FUNCTION__, " - the slot for id = ", id, " is nullptr");
        libData->Message("args");
        return {};
    } else {
        return field_grid;
    }
}

auto field_grid_library_expression_name() -> char *
{
    static std::string name{ __FUNCTION__ };
    return name.data();
}
void field_grid_library_expression_manager([[maybe_unused]] WolframLibraryData libData, mbool const mode, mint const id)
{
    if (mode == 0) { // create
        log_info(__FUNCTION__, " - reserving id = ", id);
        if (field_grid_pool.count(id) > 0)
            log_error("\tThis slot is already occupied!!");
        field_grid_pool.try_emplace(field_grid_pool.end(), id, nullptr);
    } else { // tear down
        log_info(__FUNCTION__, " - removing id = ", id);
        if (field_grid_pool.count(id) == 0)
            log_error("\tThis slot is already empty!!");
        field_grid_pool.erase(id);
    }
}

// MARK:- LibraryFunction Interfaces

/// LibraryFunction interface that returns fieldGridLibraryExpressionManager name.
///
/// It takes no argument and returns a string, "UTF8String".
///
EXTERN_C int implFieldGridLibraryExpressionManagerName(WolframLibraryData libData, mint const argc, MArgument *, MArgument const res)
{
    constexpr mint const n_args = 0;
    if (n_args != argc) {
        log_error(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        libData->Message("args");
        return LIBRARY_FUNCTION_ERROR;
    }

    MArgument_setUTF8String(res, field_grid_library_expression_name());
    return LIBRARY_NO_ERROR;
}

/// LibraryFunction interface that releases a persistent fieldGrid object associated with the passed id.
///
/// The input type specification is:
/// {id_Integer},
/// where id is the unique identifier returned from ManagedLibraryExpressionQ.
///
/// The output type specification is "Void".
///
EXTERN_C int implFieldGridRelease(WolframLibraryData libData, mint const argc, MArgument *const args, MArgument)
{
    constexpr mint n_args = 1;
    if (n_args != argc) {
        log_error(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        libData->Message("args");
        return LIBRARY_FUNCTION_ERROR;
    }

    mint id;
    if (auto const ret = get(id, libData, args[0]))
        return *ret;
    log_info(__FUNCTION__, " - id = ", id);

    return libData->releaseManagedLibraryExpression(field_grid_library_expression_name(), id);
}

/// LibraryFunction interface that constructs a persistent fieldGrid object associated with the passed id.
///
/// Input:
/// {id_Integer, tlim:{_Real, _Real}, q1lim:{_Real, _Real}, dB:{{{__Complex}..}, "Constant"}},
/// where id is the unique identifier returned from ManagedLibraryExpressionQ;
/// the real and imaginary values of dB are the two perpendicular Cartesian components.
///
/// Return: "Void"
///
EXTERN_C int implFieldGridCreate(WolframLibraryData libData, mint const argc, MArgument *const args, MArgument)
{
    constexpr mint n_args = 4;
    if (n_args != argc) {
        log_error(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        libData->Message("args");
        return LIBRARY_FUNCTION_ERROR;
    }

    mint id;
    if (auto const ret = get(id, libData, args[0]))
        return *ret;
    log_info(__FUNCTION__, " - id = ", id);

    Range t_extent;
    if (auto const ret = get(t_extent, libData, args[1]))
        return *ret;
    log_info(__FUNCTION__, " - t_extent = ", t_extent);

    Range q_extent;
    if (auto const ret = get(q_extent, libData, args[2]))
        return *ret;
    log_info(__FUNCTION__, " - q_extent = ", q_extent);

    try {
        MTensor const tensor = MArgument_getMTensor(args[3]);
        if (libData->MTensor_getRank(tensor) != 2) {
            log_error(__FUNCTION__, " - invalid rank");
            libData->Message("args");
            return LIBRARY_RANK_ERROR;
        }
        if (libData->MTensor_getType(tensor) != MType_Complex) {
            log_error(__FUNCTION__, " - invalid type");
            libData->Message("args");
            return LIBRARY_TYPE_ERROR;
        }
        auto const *dims = libData->MTensor_getDimensions(tensor);
        if (dims[0] < 2 || dims[1] < 2) {
            log_error(__FUNCTION__, " - invalid dimensions");
            libData->Message("args");
            return LIBRARY_DIMENSION_ERROR;
        }
        std::vector<CartVector> field_data(static_cast<std::size_t>(dims[0] * dims[1]));
        auto const             *data = libData->MTensor_getComplexData(tensor);
        std::transform(data, std::next(data, long(field_data.size())), begin(field_data), [](mcomplex const cx) {
            auto const vector = CartVector{ 0, mcreal(cx), mcimag(cx) };
            return vector;
        });

        if (auto const it = field_grid_pool.find(id); it == field_grid_pool.end()) {
            log_error(__FUNCTION__, " - the slot ", id, " does not exist");
            libData->Message("args");
            return LIBRARY_FUNCTION_ERROR;
        } else {
            it->second = TPT::FieldGrid::make_unique({ t_extent, q_extent }, { dims[0], dims[1] }, std::move(field_data));
            log_info(__FUNCTION__, " - FieldGrid = ", *it->second);
        }
    } catch (std::exception const &e) {
        log_error(__FUNCTION__, " - exception thrown:\n\t", e.what());
        libData->Message("exception");
        return LIBRARY_FUNCTION_ERROR;
    }

    return LIBRARY_NO_ERROR;
}

/// LibraryFunction interface that evaluates field values at given points
///
/// Input:
/// {id_Integer, time:{Real, 1, "Constant"}, pos:{Real, 1, "Constant"}},
/// where id is the unique identifier returned from ManagedLibraryExpressionQ;
///
/// Return: vCart:{Real, 2, Automatic}
///
EXTERN_C int implEvaluateFieldAt(WolframLibraryData libData, mint const argc, MArgument *const args, MArgument const res)
{
    constexpr mint n_args = 3;
    if (n_args != argc) {
        log_error(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        libData->Message("args");
        return LIBRARY_FUNCTION_ERROR;
    }

    TPT::FieldGrid const *field_grid;
    {
        mint id;
        if (auto const ret = get(id, libData, args[0]))
            return *ret;
        log_info(__FUNCTION__, " - id = ", id);

        if (auto const it = field_grid_pool.find(id); it == field_grid_pool.end()) {
            log_error(__FUNCTION__, " - the slot ", id, " does not exist");
            libData->Message("args");
            return LIBRARY_FUNCTION_ERROR;
        } else if (field_grid = it->second.get(); !field_grid) {
            log_error(__FUNCTION__, " - the slot for id = ", id, " is nullptr");
            libData->Message("args");
            return LIBRARY_FUNCTION_ERROR;
        }
    }

    std::vector<Real> time;
    {
        MTensor const tensor = MArgument_getMTensor(args[1]);
        if (libData->MTensor_getRank(tensor) != 1) {
            log_error(__FUNCTION__, " - invalid rank of time");
            libData->Message("args");
            return LIBRARY_RANK_ERROR;
        }
        if (libData->MTensor_getType(tensor) != MType_Real) {
            log_error(__FUNCTION__, " - invalid type of time");
            libData->Message("args");
            return LIBRARY_TYPE_ERROR;
        }
        auto const *data = libData->MTensor_getRealData(tensor);
        auto const *size = libData->MTensor_getDimensions(tensor);
        time             = { data, std::next(data, *size) };
        log_info(__FUNCTION__, " - size of time is ", time.size());
    }

    std::vector<Real> pos;
    {
        MTensor const tensor = MArgument_getMTensor(args[2]);
        if (libData->MTensor_getRank(tensor) != 1) {
            log_error(__FUNCTION__, " - invalid rank of pos");
            libData->Message("args");
            return LIBRARY_RANK_ERROR;
        }
        if (libData->MTensor_getType(tensor) != MType_Real) {
            log_error(__FUNCTION__, " - invalid type of pos");
            libData->Message("args");
            return LIBRARY_TYPE_ERROR;
        }
        auto const *data = libData->MTensor_getRealData(tensor);
        auto const *size = libData->MTensor_getDimensions(tensor);
        pos              = { data, std::next(data, *size) };
        log_info(__FUNCTION__, " - size of pos is ", pos.size());
    }

    if (time.size() != pos.size()) {
        log_error(__FUNCTION__, " - incompatible dimensions of time and pos");
        libData->Message("args");
        return LIBRARY_DIMENSION_ERROR;
    }

    CartVector *field;
    {
        mint const rank       = 2;
        mint const dims[rank] = { mint(time.size()), 3 };
        MTensor    tensor;
        if (auto const err = libData->MTensor_new(MType_Real, rank, dims, &tensor)) {
            log_error(__FUNCTION__, " - failed in MTensor_new");
            libData->Message("MTensor");
            return err;
        }
        auto *data = libData->MTensor_getRealData(tensor);
        field      = reinterpret_cast<CartVector *>(data);
        static_assert(sizeof(*field) == sizeof(*data) * 3);

        MArgument_setMTensor(res, tensor);
    }

    try {
        std::transform(begin(time), end(time), begin(pos), field, [field_grid](Real time, Real q1) {
            return field_grid->interp(time, CurviCoord{ q1 });
        });
    } catch (std::exception const &e) {
        log_error(__FUNCTION__, " - exception thrown:\n\t", e.what());
        libData->Message("exception");
        return LIBRARY_FUNCTION_ERROR;
    }

    return LIBRARY_NO_ERROR;
}
