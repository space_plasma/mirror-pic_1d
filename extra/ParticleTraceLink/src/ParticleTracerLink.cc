/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "ParticleTracerLink.h"
#include "MArgumentGetter.h"
#include "TraceParticle.h"

#include <PIC/UTL/Defer.h>

#include <map>
#include <memory>

namespace {
std::map<mint, std::unique_ptr<TPT::ParticleTracer const>> particle_tracer_pool{};
}
[[nodiscard]] extern auto field_grid_at(WolframLibraryData libData, mint const id) -> std::weak_ptr<TPT::FieldGrid const>;

auto particle_tracer_library_expression_name() -> char *
{
    static std::string name{ __FUNCTION__ };
    return name.data();
}
void particle_tracer_library_expression_manager([[maybe_unused]] WolframLibraryData libData, mbool const mode, mint const id)
{
    if (mode == 0) { // create
        log_info(__FUNCTION__, " - reserving id = ", id);
        if (particle_tracer_pool.count(id) > 0)
            log_error("\tThis slot is already occupied!!");
        particle_tracer_pool.try_emplace(particle_tracer_pool.end(), id, nullptr);
    } else { // tear down
        log_info(__FUNCTION__, " - removing id = ", id);
        if (particle_tracer_pool.count(id) == 0)
            log_error("\tThis slot is already empty!!");
        particle_tracer_pool.erase(id);
    }
}

// MARK:- LibraryFunction Interfaces

/// LibraryFunction interface that returns particleTracerLibraryExpressionManager name.
///
/// It takes no argument and returns a string, "UTF8String".
///
EXTERN_C int implParticleTracerLibraryExpressionManagerName(WolframLibraryData libData, mint const argc, MArgument *, MArgument const res)
{
    constexpr mint const n_args = 0;
    if (n_args != argc) {
        log_error(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        libData->Message("args");
        return LIBRARY_FUNCTION_ERROR;
    }

    MArgument_setUTF8String(res, particle_tracer_library_expression_name());
    return LIBRARY_NO_ERROR;
}

/// LibraryFunction interface that releases a persistent particleTracer object associated with the passed id.
///
/// The input type specification is:
/// {id_Integer},
/// where id is the unique identifier returned from ManagedLibraryExpressionQ.
///
/// The output type specification is "Void".
///
EXTERN_C int implParticleTracerRelease(WolframLibraryData libData, mint const argc, MArgument *const args, MArgument)
{
    constexpr mint n_args = 1;
    if (n_args != argc) {
        log_error(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        libData->Message("args");
        return LIBRARY_FUNCTION_ERROR;
    }

    mint id;
    if (auto const ret = get(id, libData, args[0]))
        return *ret;
    log_info(__FUNCTION__, " - id = ", id);

    return libData->releaseManagedLibraryExpression(particle_tracer_library_expression_name(), id);
}

/// LibraryFunction interface that constructs a persistent particleTracer object associated with the passed id.
///
/// Input:
/// {id_Integer, c_Real?Positive, xi_Real?NonNegative, D1_Real?Positive, O0_Real?Positive,
///  tlim:{_Real, _Real}, q1lim:{_Real, _Real}, bfieldID_Integer, efieldID_Integer},
/// where id is the unique identifier returned from ManagedLibraryExpressionQ.
///
/// Return: "Void"
///
EXTERN_C int implParticleTracerCreate(WolframLibraryData libData, mint const argc, MArgument *const args, MArgument)
{
    constexpr mint n_args = 9;
    if (n_args != argc) {
        log_error(__FUNCTION__, " - ", n_args, " argument(s) are expected, but ", argc, " arguments are passed");
        libData->Message("args");
        return LIBRARY_FUNCTION_ERROR;
    }

    mreal c;
    if (auto const ret = get(c, libData, args[1]))
        return *ret;
    log_info(__FUNCTION__, " - c = ", c);

    Geometry geo;
    try {
        mreal xi;
        if (auto const ret = get(xi, libData, args[2]))
            return *ret;
        log_info(__FUNCTION__, " - xi = ", xi);

        mreal D1;
        if (auto const ret = get(D1, libData, args[3]))
            return *ret;
        log_info(__FUNCTION__, " - D1 = ", D1);

        mreal O0;
        if (auto const ret = get(O0, libData, args[4]))
            return *ret;
        log_info(__FUNCTION__, " - O0 = ", O0);

        geo = { xi, D1, O0 };
    } catch (std::exception const &e) {
        log_error(__FUNCTION__, " - exception thrown:\n\t", e.what());
        libData->Message("exception");
        return LIBRARY_FUNCTION_ERROR;
    }

    Range t_extent;
    if (auto const ret = get(t_extent, libData, args[5]))
        return *ret;
    log_info(__FUNCTION__, " - t_extent = ", t_extent);

    Range q_extent;
    if (auto const ret = get(q_extent, libData, args[6]))
        return *ret;
    log_info(__FUNCTION__, " - q_extent = ", q_extent);

    std::shared_ptr<TPT::FieldGrid const> bfield;
    {
        mint id;
        if (auto const ret = get(id, libData, args[7]))
            return *ret;
        log_info(__FUNCTION__, " - bfield_id = ", id);

        if (auto field_grid = field_grid_at(libData, id).lock(); !field_grid)
            return LIBRARY_FUNCTION_ERROR;
        else
            bfield = std::move(field_grid);
    }

    std::shared_ptr<TPT::FieldGrid const> efield;
    {
        mint id;
        if (auto const ret = get(id, libData, args[8]))
            return *ret;
        log_info(__FUNCTION__, " - efield_id = ", id);

        if (auto field_grid = field_grid_at(libData, id).lock(); !field_grid)
            return LIBRARY_FUNCTION_ERROR;
        else
            efield = std::move(field_grid);
    }

    try {
        mint id;
        if (auto const ret = get(id, libData, args[0]))
            return *ret;
        log_info(__FUNCTION__, " - particle_tracer_id = ", id);

        if (auto const it = particle_tracer_pool.find(id); it == particle_tracer_pool.end()) {
            log_error(__FUNCTION__, " - the slot ", id, " does not exist");
            libData->Message("args");
            return LIBRARY_FUNCTION_ERROR;
        } else {
            it->second = TPT::ParticleTracer::make_unique(
                c, t_extent, q_extent, geo, std::move(bfield), std::move(efield));
            log_info(__FUNCTION__, " - ParticleTracer = ", *it->second);
        }
    } catch (std::exception const &e) {
        log_error(__FUNCTION__, " - exception thrown:\n\t", e.what());
        libData->Message("exception");
        return LIBRARY_FUNCTION_ERROR;
    }

    return LIBRARY_NO_ERROR;
}

/// LibraryFunction interface that traces relativistic particles.
///
/// Input:
/// {id_Integer, outFreq_Integer, tinit_Real, dt_Real,
///  Oc0_Real, gvel:{{_Real, _Real, _Real}...}, q1:{___Real}},
/// where id is the unique identifier returned from ManagedLibraryExpressionQ.
///
/// Return: {{"t" -> _Real, "gvel" -> {{_Real, _Real, _Real}...}, "q1" -> {___Real}}...}
///
EXTERN_C int implTraceRelativisticParticle(WolframLibraryData libData, WSLINK link)
{
    if (int len = 7; !WSTestHeadWithArgCount(link, "List", &len)) {
        log_error(__FUNCTION__, " - input not a list or incorrect count = ", len);
        libData->Message("linkobject");
        return void(WSNewPacket(link)), LIBRARY_FUNCTION_ERROR;
    }

    TPT::ParticleTracer const *particle_tracer;
    {
        int id;
        if (!WSGetInteger(link, &id)) {
            log_error(__FUNCTION__, " - failed to retrieve id");
            libData->Message("linkobject");
            return void(WSNewPacket(link)), LIBRARY_FUNCTION_ERROR;
        }
        log_info(__FUNCTION__, " - id = ", id);

        if (auto const it = particle_tracer_pool.find(id);
            it == particle_tracer_pool.end() || !it->second) {
            log_error(__FUNCTION__, " - failed to retrieve ParticleTracer at id = ", id);
            libData->Message("linkobject");
            return void(WSNewPacket(link)), LIBRARY_FUNCTION_ERROR;
        } else {
            particle_tracer = it->second.get();
        }
    }

    int out_freq;
    if (!WSGetInteger(link, &out_freq)) {
        log_error(__FUNCTION__, " - failed to retrieve output frequency");
        libData->Message("linkobject");
        return void(WSNewPacket(link)), LIBRARY_FUNCTION_ERROR;
    }
    log_info(__FUNCTION__, " - output frequency = ", out_freq);

    Real tinit;
    if (!WSGetReal(link, &tinit)) {
        log_error(__FUNCTION__, " - failed to retrieve tinit");
        libData->Message("linkobject");
        return void(WSNewPacket(link)), LIBRARY_FUNCTION_ERROR;
    }
    log_info(__FUNCTION__, " - tinit = ", tinit);

    Real dt;
    if (!WSGetReal(link, &dt)) {
        log_error(__FUNCTION__, " - failed to retrieve dt");
        libData->Message("linkobject");
        return void(WSNewPacket(link)), LIBRARY_FUNCTION_ERROR;
    }
    log_info(__FUNCTION__, " - dt = ", dt);

    mreal Oc0;
    if (!WSGetReal(link, &Oc0)) {
        log_error(__FUNCTION__, " - failed to retrieve Oc0");
        libData->Message("linkobject");
        return void(WSNewPacket(link)), LIBRARY_FUNCTION_ERROR;
    }
    log_info(__FUNCTION__, " - Oc0 = ", Oc0);

    std::vector<TPT::RelativisticParticle> particles;
    {
        mreal *gvel_data;
        long  *gvel_dims;
        char **gvel_heads;
        long   gvel_rank;
        if (!WSGetRealArray(link, &gvel_data, &gvel_dims, &gvel_heads, &gvel_rank)) {
            log_error(__FUNCTION__, " - failed to read gvel list of vectors");
            libData->Message("linkobject");
            return void(WSNewPacket(link)), LIBRARY_FUNCTION_ERROR;
        }
        LIBPIC_DEFER(WSReleaseRealArray, link, gvel_data, gvel_dims, gvel_heads, gvel_rank);
        log_info(__FUNCTION__, " - gvel rank = ", gvel_rank);

        mreal *pos_data;
        long   pos_size;
        if (!WSGetRealList(link, &pos_data, &pos_size)) {
            log_error(__FUNCTION__, " - failed to read pos list");
            libData->Message("linkobject");
            return void(WSNewPacket(link)), LIBRARY_FUNCTION_ERROR;
        }
        LIBPIC_DEFER(WSReleaseRealList, link, pos_data, pos_size);
        log_info(__FUNCTION__, " - pos size = ", pos_size);

        if (gvel_rank != 2 || gvel_dims[1] != 3) {
            log_error(__FUNCTION__, " - gvel_p dimension error");
            libData->Message("args");
            return void(WSNewPacket(link)), LIBRARY_DIMENSION_ERROR;
        }
        if (gvel_dims[0] != pos_size) {
            log_error(__FUNCTION__, " - Length[gvel_p] != Length[pos]");
            libData->Message("args");
            return void(WSNewPacket(link)), LIBRARY_DIMENSION_ERROR;
        }

        particles.reserve(static_cast<std::size_t>(pos_size));
        auto const *gvel_p = reinterpret_cast<CartVector const *>(gvel_data);
        for (long i = 0; i < pos_size; ++i) {
            auto const gcgvel = lorentz_boost<-1>(FourCartVector{ particle_tracer->c, {} }, gvel_p[i] / particle_tracer->c);
            auto const pos    = CurviCoord{ pos_data[i] };
            particles.emplace_back(Oc0, gcgvel, pos);
        }
    }

    // calculation
    std::vector<SingleParticleResult> result;
    try {
        result = trace(libData->AbortQ, *particle_tracer, out_freq, tinit, dt, particles);
    } catch (std::exception const &e) {
        log_error(__FUNCTION__, " - exception thrown:\n\t", e.what());
        libData->Message("exception");
        return void(WSNewPacket(link)), LIBRARY_FUNCTION_ERROR;
    }

    // return
    if (!WSNewPacket(link)) {
        log_error(__FUNCTION__, " - failed to establish WSNewPacket");
        libData->Message("linkobject");
        return LIBRARY_FUNCTION_ERROR;
    }
    if (libData->AbortQ()) {
        WSPutFunction(link, "Abort", 0);
        return LIBRARY_NO_ERROR;
    }

    if (!WSPutFunction(link, "CompoundExpression", 2)) {
        log_error(__FUNCTION__, " - failed to open CompoundExpression");
        libData->Message("linkobject");
        return LIBRARY_FUNCTION_ERROR;
    }

    // intermediate state
    int count = static_cast<int>(result.size());
    if (!WSPutFunction(link, "CompoundExpression", count)) {
        log_error(__FUNCTION__, " - failed to open CompoundExpression with nargs = ", count);
        libData->Message("linkobject");
        return LIBRARY_FUNCTION_ERROR;
    }
    for (std::size_t i = 0, size = result.size(); i < size; ++i) {
        if (!WSPutFunction(link, "Sow", 1)) {
            log_error(__FUNCTION__, " - failed to open Sow");
            libData->Message("linkobject");
            return LIBRARY_FUNCTION_ERROR;
        }
        if (!WSPutFunction(link, "Association", 3)) {
            log_error(__FUNCTION__, " - failed to open Association of length = ", 3);
            libData->Message("linkobject");
            return LIBRARY_FUNCTION_ERROR;
        }

        auto const &entry = result[i];
        if (!WSPutFunction(link, "Rule", 2) || !WSPutString(link, "t")
            || !WSPutRealList(link, entry.time.data(), long(entry.time.size()))) {
            log_error(__FUNCTION__, " - failed to put \"t\" -> {<<", entry.time.size(), ">>} at i = ", i);
            libData->Message("linkobject");
            return LIBRARY_FUNCTION_ERROR;
        }
        if (!WSPutFunction(link, "Rule", 2) || !WSPutString(link, "q1")
            || !WSPutRealList(link, reinterpret_cast<Real const *>(entry.pos.data()), long(entry.pos.size()))) {
            log_error(__FUNCTION__, " - failed to put \"pos\" -> {<<", entry.pos.size(), ">>} at i = ", i);
            libData->Message("linkobject");
            return LIBRARY_FUNCTION_ERROR;
        }
        long const rank       = 2;
        long const dims[rank] = { long(entry.gvel.size()), 3 };
        if (!WSPutFunction(link, "Rule", 2) || !WSPutString(link, "gvel")
            || !WSPutRealArray(link, reinterpret_cast<Real const *>(entry.gvel.data()), dims, nullptr, rank)) {
            log_error(__FUNCTION__, " - failed to put \"gvel\" -> {<<", entry.gvel.size(), ">>} at i = ", i);
            libData->Message("linkobject");
            return LIBRARY_FUNCTION_ERROR;
        }
    }

    // last state
    {
        long const rank       = 2;
        long const dims[rank] = { long(particles.size()), 6 }; // inner element = {Oc0, gc, gvx, gvy, gvz, q1}
        static_assert(sizeof(TPT::RelativisticParticle) == 6 * sizeof(mreal));
        if (!WSPutRealArray(link, reinterpret_cast<mreal const *>(particles.data()), dims, nullptr, rank)) {
            log_error(__FUNCTION__, " - failed to put particle results, dims = {", dims[0], ", ", dims[1], "}");
            libData->Message("linkobject");
            return LIBRARY_FUNCTION_ERROR;
        }
    }

    return LIBRARY_NO_ERROR;
}
