/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "TraceParticle.h"

#include <atomic>
#include <chrono>
#include <functional>
#include <future>

#if !defined(__OBJC__)
#error("compile with objective-c++")
#else
#import <dispatch/dispatch.h>
#endif

namespace {
[[nodiscard]] auto impl_trace_single(TPT::ParticleTracer const &particle_tracer, long const out_freq, Real const tinit, Real const dt,
                                     TPT::RelativisticParticle &particle) -> SingleParticleResult
{
    SingleParticleResult result;
    auto const           last
        = particle_tracer.trace(tinit, particle, dt, [out_freq, &result](long i, Real t, auto const &particle) {
              if (out_freq > 0 && i % out_freq == 0)
                  result.push_back(t, particle);
              return true;
          });
    particle = last.second;
    return result;
}
auto impl_trace(std::atomic<bool> &abortQ, TPT::ParticleTracer const &particle_tracer, long const out_freq, Real const tinit, Real const dt,
                std::vector<TPT::RelativisticParticle> &particles) -> std::vector<SingleParticleResult>
{
    std::vector<SingleParticleResult> result(particles.size());
    @autoreleasepool {
        auto *result_ptr = &result;
        dispatch_apply(
            particles.size(), dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0),
            ^(std::size_t const idx) {
                if (!abortQ.load())
                    result_ptr->at(idx) = impl_trace_single(particle_tracer, out_freq, tinit, dt, particles.at(idx));
            });
    }
    if (out_freq > 0)
        return result;
    return {};
}
} // namespace

auto trace(mint (*AbortQ)(), TPT::ParticleTracer const &particle_tracer, long const out_freq, Real const tinit, Real const dt,
           std::vector<TPT::RelativisticParticle> &particles) -> std::vector<SingleParticleResult>
{
    std::atomic<bool> abortQ{ false };

    auto future = std::async(std::launch::async, &impl_trace, std::ref(abortQ), std::cref(particle_tracer), out_freq, tinit, dt, std::ref(particles));

    std::future_status status;
    do {
        using namespace std::chrono_literals;
        if (status = future.wait_for(1s); std::future_status::ready != status) {
            if ((*AbortQ)()) {
                abortQ.store(true);
                future.wait();
                particles = {};
                return {};
            }
        }
    } while (std::future_status::ready != status);

    return future.get();
}
