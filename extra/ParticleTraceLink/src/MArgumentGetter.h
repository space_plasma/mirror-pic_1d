/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include "Common.h"

#include <TPT/Config.h>

#include <PIC/UTL/Range.h>

namespace {
struct ErrorCode {
    int code;

    ErrorCode(int code) noexcept
    : code{ code } {}

    [[nodiscard]] auto operator*() const
    {
        return code;
    }
    explicit operator bool() const
    {
        return code != LIBRARY_NO_ERROR;
    }
};
[[nodiscard, maybe_unused]] ErrorCode get(mint &value, [[maybe_unused]] WolframLibraryData libData, MArgument const arg)
{
    value = MArgument_getInteger(arg);
    return LIBRARY_NO_ERROR;
}
[[nodiscard, maybe_unused]] ErrorCode get(mreal &value, [[maybe_unused]] WolframLibraryData libData, MArgument const arg)
{
    value = MArgument_getReal(arg);
    return LIBRARY_NO_ERROR;
}
[[nodiscard, maybe_unused]] ErrorCode get(Range &value, [[maybe_unused]] WolframLibraryData libData, MArgument const arg)
{
    MTensor const tensor = MArgument_getMTensor(arg);
    if (libData->MTensor_getRank(tensor) != 1) {
        log_error("\t - invalid rank of Range");
        libData->Message("args");
        return LIBRARY_RANK_ERROR;
    }
    if (*(*libData->MTensor_getDimensions)(tensor) != 2) {
        log_error("\t - invalid dimensions of Range");
        libData->Message("args");
        return LIBRARY_DIMENSION_ERROR;
    }
    if (libData->MTensor_getType(tensor) != MType_Real) {
        log_error("\t - invalid type of Range");
        libData->Message("args");
        return LIBRARY_TYPE_ERROR;
    }
    mreal const *data = libData->MTensor_getRealData(tensor);
    value.loc         = data[0];
    value.len         = data[1] - data[0];

    return LIBRARY_NO_ERROR;
}
} // namespace
