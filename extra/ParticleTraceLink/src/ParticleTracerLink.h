/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include "Common.h"

extern auto particle_tracer_library_expression_name() -> char *;
extern void particle_tracer_library_expression_manager(WolframLibraryData, mbool, mint);
