/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "Common.h"
#include "FieldGridLink.h"
#include "ParticleTracerLink.h"

#include <string_view>

EXTERN_C mint WolframLibrary_getVersion()
{
    return WolframLibraryVersion;
}

void define_LibraryFunction_tags(WolframLibraryData libData, std::string_view const tag, std::string_view const msg)
{
    log_info(__FUNCTION__, " - defining LibraryFunction::", tag);

    WSLINK link = libData->getWSLINK(libData);
    WSPutFunction(link, "EvaluatePacket", 1);
    WSPutFunction(link, "Set", 2);
    {
        WSPutFunction(link, "MessageName", 2);
        {
            WSPutSymbol(link, "LibraryFunction");
            WSPutString(link, tag.data());
        }
        WSPutString(link, msg.data());
    }
    if (int const success = libData->processWSLINK(link); success == 0) {
        log_error(__FUNCTION__, " - evaluation failure: tag = ", tag);
        return;
    }
    if (WSNextPacket(link) == RETURNPKT)
        WSNewPacket(link);
}
EXTERN_C int WolframLibrary_initialize(WolframLibraryData libData)
{
    // define LibraryFunction tags
    define_LibraryFunction_tags(libData, "args", "invalid argument");
    define_LibraryFunction_tags(libData, "exception", "exception thrown");
    define_LibraryFunction_tags(libData, "linkobject", "error on working with LinkObject");
    define_LibraryFunction_tags(libData, "MTensor", "error on working with MTensor");

    // register managers
    libData->registerLibraryExpressionManager(particle_tracer_library_expression_name(), &particle_tracer_library_expression_manager);
    libData->registerLibraryExpressionManager(field_grid_library_expression_name(), &field_grid_library_expression_manager);
    // libData->registerLibraryCallbackManager(FindRootCallbackManagerName(), &FindRootCallbackManager);

    return 0;
}

EXTERN_C void WolframLibrary_uninitialize(WolframLibraryData libData)
{
    // unregister managers
    libData->unregisterLibraryExpressionManager(field_grid_library_expression_name());
    libData->unregisterLibraryExpressionManager(particle_tracer_library_expression_name());
    // libData->unregisterLibraryCallbackManager(FindRootCallbackManagerName());
}
