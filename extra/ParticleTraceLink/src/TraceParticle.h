/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <TPT/ParticleTracer.h>

#include "Common.h"

struct SingleParticleResult {
    std::vector<Real>       time;
    std::vector<CartVector> gvel;
    std::vector<CurviCoord> pos;

    void push_back(Real const time, TPT::RelativisticParticle const &particle) &
    {
        this->time.push_back(time);
        this->gvel.push_back(particle.gcgvel.s);
        this->pos.push_back(particle.pos);
    }
};

[[nodiscard]] auto trace(mint (*AbortQ)(), TPT::ParticleTracer const &, long out_freq, Real tinit, Real dt, std::vector<TPT::RelativisticParticle> &)
    -> std::vector<SingleParticleResult>;
