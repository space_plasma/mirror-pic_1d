/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <iostream>
#include <type_traits>

#include <wstp.h>

#include <WolframLibrary.h>

// debug print
//
namespace {
template <class... Args>
void log_error([[maybe_unused]] Args &&...args)
{
    ((std::cerr << "ParticleTraceLink::debug : ") << ... << std::forward<Args>(args)) << std::endl;
}
template <class... Args>
void log_info([[maybe_unused]] Args &&...args)
{
#if defined(DEBUG)
    ((std::cerr << "ParticleTraceLink::info : ") << ... << std::forward<Args>(args)) << std::endl;
#endif
}
} // namespace
