(* ::Package:: *)

tracer =
    With[{c = 8., O0 = 1., xi = 0.05, D1 = 0.10725, Nx = 120 * 2, dt
        = 0.01, innerNt = 10, outerNt = 200},
        Module[{tlim, qlim, efield, tracer},
            tlim = {-(innerNt * outerNt + 0.1) * dt, (innerNt * outerNt
                 + 0.1) * dt} // Interval // MinMax;
            qlim = {-1, 1} * Nx / 2. // Interval // MinMax;
            bfield = TPTFieldGridCreate[tlim, qlim, ConstantArray[0.
                I, {2, 2}]];
            efield = TPTFieldGridCreate[tlim, qlim, ConstantArray[0.
                I, {2, 2}]];
            tracer = TPTParticleTracerCreate[c, {xi, D1, O0}, tlim, qlim,
                 bfield, efield];
            tracer
        ]
    ]


result =
    With[{q1 = {0.}, gvel = {{5.07535, 8.79076, 0.}}, Oc0 = 2. Pi, outFreq
         = 10, tinit = 0., dt = 0.01},
        Reap[TPTTraceRelativisticParticles[tracer, tinit, dt, Oc0, gvel,
             q1, "OutputFrequency" -> outFreq]]
    ]


TPTEvaluateFieldAt[bfield, {-1., 2.}, {-0.5, 1.5}]
