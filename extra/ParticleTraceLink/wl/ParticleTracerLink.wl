(* ::Package:: *)

 (*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 *)


BeginPackage["ParticleTraceLink`"]


Unprotect[TPTParticleTracerLibraryExpressionID, TPTParticleTracerLibraryExpressionQ,
     TPTParticleTracerRelease, TPTParticleTracerCreate, TPTTraceRelativisticParticles
    ]

ClearAll[TPTParticleTracerLibraryExpressionID, TPTParticleTracerLibraryExpressionQ,
     TPTParticleTracerRelease, TPTParticleTracerCreate, TPTTraceRelativisticParticles
    ]

TPTParticleTracerLibraryExpressionID::usage = "TPTParticleTracerLibraryExpressionID[e] returns the identifier of the managed library expression, e."

TPTParticleTracerLibraryExpressionQ::usage = "TPTParticleTracerLibraryExpressionQ[e] tests whether e is a valid managed library expression."

TPTParticleTracerRelease::usage = "TPTParticleTracerRelease[e] releases the managed library expression."

TPTParticleTracerCreate::usage = "TPTParticleTracerCreate[c, {xi, D1, O0}, tlim, q1lim, be, ee] creates a persistent ParticleTracer managed object given bfield and efield managed library expressions.
TPTParticleTracerCreate[c, {xi, D1, O0}, tlim, q1lim, bfield, efield] creates a persistent ParticleTracer managed object given bfield and efield."

TPTTraceRelativisticParticles::usage = "TPTTraceRelativisticParticles[e, tinit, dt, Oc0, gvel, q1] traces relativistic particles, where Oc0 is a scalar, and gvel is list of vectors and q1 is list of q1 coordinates.
It returns the final state of particles wrapped in an Association with keys {\"Oc0\", \"gc\", \"gvx\", \"gvy\", \"gvz\", \"q1\"}.
The \"OutputFrequency\" option, when specified with a positive integer, instructs to \"Sow\" intermediate results as a list of Associations."


Begin["`Private`"]


libPath = FileNameJoin[{FileNameDrop[FindFile["ParticleTraceLink`"],
    -1](*, "LibraryResources", $SystemID*)}]

libName = "libParticleTraceLink.dylib"

implParticleTracerLibraryExpressionManagerName =
    Block[{$LibraryPath = libPath},
        LibraryFunctionLoad[libName, "implParticleTracerLibraryExpressionManagerName",
             {}, "UTF8String"]
    ]

implParticleTracerRelease =
    Block[{$LibraryPath = libPath},
        LibraryFunctionLoad[libName, "implParticleTracerRelease", {Integer
            }, "Void"]
    ]

implParticleTracerCreate =
    Block[{$LibraryPath = libPath},
        LibraryFunctionLoad[
            libName
            ,
            "implParticleTracerCreate"
            ,
            {(*id:*)Integer
                ,(*c:*)Real
                ,(*xi:*)Real
                ,(*D1:*)Real
                ,(*O0:*)Real
                ,
                (*tlim:*){Real, 1, "Constant"}
                ,(*q1lim:*){Real, 1, "Constant"}
                ,(*dB_ID:*)Integer
                , (*dE_ID:*) Integer
            }
            ,
            "Void"
        ]
    ]

implTraceRelativisticParticle =
    Block[{$LibraryPath = libPath},
        LibraryFunctionLoad[libName, "implTraceRelativisticParticle",
             LinkObject, LinkObject]
    ]


TPTParticleTracerLibraryExpressionID[e_] :=
    ManagedLibraryExpressionID[e, implParticleTracerLibraryExpressionManagerName[
        ]]

TPTParticleTracerLibraryExpressionQ[e_] :=
    ManagedLibraryExpressionQ[e, implParticleTracerLibraryExpressionManagerName[
        ]]

TPTParticleTracerRelease[e_?TPTParticleTracerLibraryExpressionQ] :=
    implParticleTracerRelease[e // TPTParticleTracerLibraryExpressionID
        ]

TPTParticleTracerCreate[c_Real?Positive, {xi_Real?NonNegative, D1_Real
    ?Positive, O0_Real?Positive}, tlim : {_Real, _Real} /; Negative[Subtract
     @@ tlim], q1lim : {_Real, _Real} /; Negative[Subtract @@ q1lim], bfield_
    ?TPTFieldGridLibraryExpressionQ, efield_?TPTFieldGridLibraryExpressionQ
    ] :=
    Module[{wrapper, e},
        e = CreateManagedLibraryExpression[implParticleTracerLibraryExpressionManagerName[
            ], wrapper];
        implParticleTracerCreate[e // TPTParticleTracerLibraryExpressionID,
             c, xi, D1, O0, tlim, q1lim, TPTFieldGridLibraryExpressionID[bfield],
             TPTFieldGridLibraryExpressionID[efield]];
        e
    ]

TPTParticleTracerCreate[c_Real?Positive, {xi_Real?NonNegative, D1_Real
    ?Positive, O0_Real?Positive}, tlim : {_Real, _Real} /; Negative[Subtract
     @@ tlim], q1lim : {_Real, _Real} /; Negative[Subtract @@ q1lim], bfield
     : {{_Real, _Real}, {_Real, _Real}, {{__Complex}..}}, efield : {{_Real,
     _Real}, {_Real, _Real}, {{__Complex}..}}] :=
    Module[{be, ee, e},
        be = Apply[TPTFieldGridCreate, bfield];
        If[!TPTFieldGridLibraryExpressionQ[be],
            (
                Print["Failed to create dB."];
                Abort[]
            )
        ];
        ee = Apply[TPTFieldGridCreate, efield];
        If[!TPTFieldGridLibraryExpressionQ[ee],
            (
                Print["Failed to create dE."];
                Abort[]
            )
        ];
        e = TPTParticleTracerCreate[c, {xi, D1, O0}, tlim, q1lim, be,
             ee];
        e
    ]

Options[TPTTraceRelativisticParticles] = {"OutputFrequency" -> 0}

TPTTraceRelativisticParticles[e_?TPTParticleTracerLibraryExpressionQ,
     tinit_Real, dt_Real, Oc0_Real, gvel : {{_Real, _Real, _Real}..}, q1
    : {__Real}, OptionsPattern[]] /; Length[gvel] == Length[q1] :=
    Module[{result, outFreq = OptionValue["OutputFrequency"]},
        result = implTraceRelativisticParticle[e // TPTParticleTracerLibraryExpressionID,
             outFreq, tinit, dt, Oc0, gvel, q1];
        Association[Thread[Rule[{"Oc0", "gc", "gvx", "gvy", "gvz", "q1"}, Thread[result]]]]
    ]


End[] (*`Private`*)


Protect[TPTParticleTracerLibraryExpressionID, TPTParticleTracerLibraryExpressionQ,
     TPTParticleTracerRelease, TPTParticleTracerCreate, TPTTraceRelativisticParticles
    ]


EndPackage[]
