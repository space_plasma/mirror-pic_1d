(* ::Package:: *)

 (*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 *)


BeginPackage["ParticleTraceLink`"]


Unprotect[TPTFieldGridLibraryExpressionID, TPTFieldGridLibraryExpressionQ,
     TPTFieldGridRelease, TPTFieldGridCreate, TPTEvaluateFieldAt]

ClearAll[TPTFieldGridLibraryExpressionID, TPTFieldGridLibraryExpressionQ,
     TPTFieldGridRelease, TPTFieldGridCreate, TPTEvaluateFieldAt]

TPTFieldGridLibraryExpressionID::usage = "TPTFieldGridLibraryExpressionID[e] returns the identifier of the managed library expression, e."

TPTFieldGridLibraryExpressionQ::usage = "TPTFieldGridLibraryExpressionQ[e] tests whether e is a valid managed library expression."

TPTFieldGridRelease::usage = "TPTFieldGridRelease[e] releases the managed library expression."

TPTFieldGridCreate::usage = "TPTFieldGridCreate[tlim, q1lim, field] creates a persistent FieldGrid managed object given time interval, q1 interval,
and complex-valued 2D array of field representing two Cartesian components of the field vector."

TPTEvaluateFieldAt::usage = "TPTEvaluateFieldAt[e, time, pos] evaluates field at given time and position."


Begin["`Private`"]


libPath = FileNameJoin[{FileNameDrop[FindFile["ParticleTraceLink`"],
    -1](*, "LibraryResources", $SystemID*)}]

libName = "libParticleTraceLink.dylib"

implFieldGridLibraryExpressionManagerName =
    Block[{$LibraryPath = libPath},
        LibraryFunctionLoad[libName, "implFieldGridLibraryExpressionManagerName",
             {}, "UTF8String"]
    ]

implFieldGridRelease =
    Block[{$LibraryPath = libPath},
        LibraryFunctionLoad[
            libName
            ,
            "implFieldGridRelease"
            ,
            {(*id:*)Integer
            }
            ,
            "Void"
        ]
    ]

implFieldGridCreate =
    Block[{$LibraryPath = libPath},
        LibraryFunctionLoad[
            libName
            ,
            "implFieldGridCreate"
            ,
            {(*id:*)Integer
                ,(*tlim:*){Real, 1, "Constant"}
                ,(*q1lim:*)
                {Real, 1, "Constant"}
                ,(*dB:*){Complex, 2, "Constant"}
            }
            ,
            "Void"
        ]
    ]

implEvaluateFieldAt =
    Block[{$LibraryPath = libPath},
        LibraryFunctionLoad[
            libName
            ,
            "implEvaluateFieldAt"
            ,
            {(*id:*)Integer
                ,(*time:*) {Real, 1, "Constant"}
                , (*pos:*)
                {Real, 1, "Constant"}
            }
            ,
            {Real, 2, Automatic}
        ]
    ]


TPTFieldGridLibraryExpressionQ[e_] :=
    ManagedLibraryExpressionQ[e, implFieldGridLibraryExpressionManagerName[
        ]]

TPTFieldGridLibraryExpressionID[e_] :=
    ManagedLibraryExpressionID[e, implFieldGridLibraryExpressionManagerName[
        ]]

TPTFieldGridRelease[e_?TPTFieldGridLibraryExpressionQ] :=
    implFieldGridRelease[e // ManagedLibraryExpressionID]

TPTFieldGridCreate[tlim : {_Real, _Real} /; Negative[Subtract @@ tlim
    ], q1lim : {_Real, _Real} /; Negative[Subtract @@ q1lim], field : {{__Complex
    }..} /; Apply[And, Thread[Dimensions[field] >= 2]]] :=
    Module[{wrapper, e},
        e = CreateManagedLibraryExpression[implFieldGridLibraryExpressionManagerName[
            ], wrapper];
        implFieldGridCreate[e // TPTFieldGridLibraryExpressionID, tlim,
             q1lim, N[field]];
        e
    ]

TPTEvaluateFieldAt[e_?TPTFieldGridLibraryExpressionQ, time : {__Real},
     pos : {__Real}] /; Length[time] == Length[pos] :=
    With[{},
        implEvaluateFieldAt[e // TPTFieldGridLibraryExpressionID, time,
             pos]
    ]


End[] (*`Private`*)


Protect[TPTFieldGridLibraryExpressionID, TPTFieldGridLibraryExpressionQ,
     TPTFieldGridRelease, TPTFieldGridCreate, TPTEvaluateFieldAt]


EndPackage[]
