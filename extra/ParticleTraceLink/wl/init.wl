(* ::Package:: *)

 (*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 *)


(* The order of declaration is important *)

Get[FileNameJoin[{FileNameDrop[FindFile["ParticleTraceLink`"], -1], "FieldGridLink.wl"
    }]]

Get[FileNameJoin[{FileNameDrop[FindFile["ParticleTraceLink`"], -1], "ParticleTracerLink.wl"
    }]]
