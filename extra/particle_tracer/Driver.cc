/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "Driver.h"
#include "FieldLoader.h"

#include <PIC/UTL/println.h>

#include <chrono>
#include <functional>
#include <iostream>
#include <map>
#include <stdexcept>
#include <string_view>
#include <utility>
#include <variant>

LIBTPT_NAMESPACE_BEGIN
namespace {
template <class F, class... Args>
[[nodiscard]] auto measure(F &&f, Args &&...args) -> std::chrono::duration<double>
{
    static_assert(std::is_invocable_v<F &&, Args &&...>);
    auto const start = std::chrono::steady_clock::now();
    {
        std::invoke(std::forward<F>(f), std::forward<Args>(args)...);
    }
    auto const end = std::chrono::steady_clock::now();

    return end - start;
}
} // namespace

Driver::~Driver() = default;
Driver::Driver(parallel::mpi::Comm _world, Options const &opts)
: result{ std::move(_world) }
{
    // commandline argument parse
    auto const &[bfield_path, efield_path, particle_path] = [&opts, this] {
        std::string bfield;
        std::string efield;
        std::string particle;
        std::string output;

        std::map<std::string_view, std::variant<std::string *, int *>> map{
            { "number_of_mpi_processes", &number_of_mpi_processes },
            { "number_of_threads", &number_of_threads },
            { "bfield", &bfield },
            { "efield", &efield },
            { "particle", &particle },
            { "output", &output },
        };
        for (auto const &[key, val] : *opts) {
            std::visit(val, map.at(key));
        }

        output_path = output;
        return std::make_tuple(
            std::filesystem::path{ bfield },
            std::filesystem::path{ efield },
            std::filesystem::path{ particle });
    }();
    if (auto const mpi_size = result.world.size(); mpi_size != number_of_mpi_processes)
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ } + " - number_of_mpi_processes (" + std::to_string(number_of_mpi_processes) + ") != MPI_COMM_WORLD.size (" + std::to_string(mpi_size) + ")" };
    if (number_of_threads <= 0)
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ } + " - invalid number_of_threads: " + std::to_string(number_of_threads) };
    if (bfield_path.empty())
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ } + " - empty \"bfield\" path" };
    if (efield_path.empty())
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ } + " - empty \"efield\" path" };
    if (particle_path.empty())
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ } + " - empty \"particle\" path" };
    if (output_path.empty())
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ } + " - empty \"output\" path" };

    // load data
    result << particle_path;
    tracer = ParticleTracer::make_unique(
        result->c, result.time_extent, result.space_extent, result.geomtr,
        *FieldLoader{ bfield_path }, *FieldLoader{ efield_path });
    if (!tracer)
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ } + " - failed to instantiate tracer" };
}

void Driver::operator()() &&
{
    auto const elapsed = measure(std::mem_fn(&Driver::do_work), this);
    if (0 == result.world.rank())
        println(std::cout, "%% time elapsed: ", elapsed.count(), 's');
}
void Driver::do_work() &
{
    result.trace(*tracer, number_of_threads);
    result.collect();
    std::move(result) >> output_path;
}
LIBTPT_NAMESPACE_END
