/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "FieldLoader.h"

#include <HDF5Kit/HDF5Kit.h>

#include <stdexcept>
#include <string>
#include <utility>

LIBTPT_NAMESPACE_BEGIN
FieldLoader::~FieldLoader() = default;

namespace {
[[nodiscard]] auto load_field(hdf5::Group const &root)
{
    auto const get_extent = [&root](std::string_view const name) {
        std::array<Real, 2> lim{};
        root.attribute(name.data()).read(lim.data(), hdf5::make_type(*lim.data()));
        if (std::get<0>(lim) >= std::get<1>(lim))
            throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ } + " - invalid " + name.data() + ": {" + std::to_string(lim[0]) + ", " + std::to_string(lim[1]) + "}" };
        return Range{ lim.front(), lim.back() - lim.front() };
    };
    auto const time_extent  = get_extent("tlim");
    auto const space_extent = get_extent("q1lim");

    auto const dset  = root.dataset("data");
    auto       space = dset.space();
    auto const dims  = space.simple_extent().first;
    if (auto const rank = dims.rank(); rank != 3)
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ } + " - invalid rank of field data: rank = " + std::to_string(rank) };
    else if (auto const vlen = dims[2]; vlen != 3)
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ } + " - invalid vector size of field data: len = " + std::to_string(vlen) };

    space.select_all();
    std::vector<CartVector> field(dims[0] * dims[1]);
    dset.read(space, reinterpret_cast<Real *>(field.data()), space);

    return FieldGrid::make_unique({ time_extent, space_extent }, { long(dims[0]), long(dims[1]) }, std::move(field));
}
} // namespace
FieldLoader::FieldLoader(std::filesystem::path const path)
{
    auto const root = [path] {
        using namespace hdf5;
        auto const file = File{ File::rdonly_tag{}, path.c_str() };
        return file.group(gname.data());
    }();

    m_field = load_field(root);
}
LIBTPT_NAMESPACE_END
