/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <TPT/ParticleTracer.h>

#include <PIC/Geometry.h>
#include <PIC/UTL/Range.h>

#include <ParallelKit/ParallelKit.h>

#include <algorithm>
#include <filesystem>
#include <limits>
#include <ostream>
#include <string_view>
#include <vector>

LIBTPT_NAMESPACE_BEGIN
class TraceResult {
    static constexpr auto gname     = std::string_view{ "particle" };
    static constexpr auto quiet_nan = std::numeric_limits<Real>::quiet_NaN();

public:
    ~TraceResult();

    TraceResult(TraceResult const &)     = delete;
    TraceResult(TraceResult &&) noexcept = default;
    TraceResult &operator=(TraceResult const &) = delete;
    TraceResult &operator=(TraceResult &&) noexcept = default;

    explicit TraceResult(parallel::mpi::Comm _world)
    : world{ std::move(_world) }
    {
        master = parallel::mpi::Rank{ world.size() - 1 };
    }

    // load initial particles
    void operator<<(std::filesystem::path) &;

    // export final particles
    void operator>>(std::filesystem::path) &&;

    // trace
    void trace(ParticleTracer const &, int number_of_threads) &;
    void collect() &;

    [[nodiscard]] auto *operator->() const noexcept
    {
        return &param;
    }

    struct {
        Real c      = quiet_nan;
        Real dt     = quiet_nan;
        Real tinit  = quiet_nan;
        Real tfinal = quiet_nan;
        Real Oc0    = quiet_nan;
    } param;
    Geometry geomtr;
    Range    time_extent;
    Range    space_extent;

    parallel::mpi::Comm world;
    parallel::mpi::Rank master;

private:
    struct Pack {
        std::vector<CurviCoord> pos;
        std::vector<CartVector> gvel;
        std::vector<CartVector> bfield;
        std::vector<CartVector> efield;

        void trim(long first, long last) &
        {
            pos.erase(std::next(begin(pos), last), end(pos));
            pos.erase(begin(pos), std::next(begin(pos), first));

            gvel.erase(std::next(begin(gvel), last), end(gvel));
            gvel.erase(begin(gvel), std::next(begin(gvel), first));

            bfield.erase(std::next(begin(bfield), last), end(bfield));
            bfield.erase(begin(bfield), std::next(begin(bfield), first));

            efield.erase(std::next(begin(efield), last), end(efield));
            efield.erase(begin(efield), std::next(begin(efield), first));
        }
    } first, last;

    [[nodiscard]] Real impl_trace(bool is_master, ParticleTracer const &, std::size_t index_first, std::size_t index_last, Pack *last) const &;

    template <class Comm, class T>
    [[nodiscard]] auto gather(Comm const &, std::vector<T>, int const tag);

    template <class CharT, class Traits>
    friend decltype(auto) operator<<(std::basic_ostream<CharT, Traits> &os, TraceResult const &result)
    {
        os << "{\n";

        // attributes
        os << "\t\"c\" -> " << result->c << ",\n";
        os << "\t\"dt\" -> " << result->dt << ",\n";
        os << "\t\"tinit\" -> " << result->tinit << ",\n";
        os << "\t\"tfinal\" -> " << result->tfinal << ",\n";
        os << "\t\"Oc0\" -> " << result->Oc0 << ",\n";
        os << "\t\"xi\" -> " << result.geomtr.xi() << ",\n";
        os << "\t\"D\" -> " << result.geomtr.D() << ",\n";
        os << "\t\"O0\" -> " << result.geomtr.B0() << ",\n";
        os << "\t\"time_extent\" -> " << result.time_extent << ",\n";
        os << "\t\"space_extent\" -> " << result.space_extent << ",\n";

        if (result.first.pos.empty()) {
            os << "\t\"first.pos\" -> {},\n";
            os << "\t\"first.gvel\" -> {}\n}";
        } else {
            os << "\t\"first.pos\" -> {\n\t\t" << result.first.pos.front();
            std::for_each(std::next(begin(result.first.pos)), end(result.first.pos), [&os](CurviCoord const &pos) {
                os << ",\n\t\t" << pos;
            });
            os << "\n\t},\n";

            os << "\t\"first.gvel\" -> {\n\t\t" << result.first.gvel.front();
            std::for_each(std::next(begin(result.first.gvel)), end(result.first.gvel), [&os](CartVector const &gvel) {
                os << ",\n\t\t" << gvel;
            });
            os << "\n\t}\n}";
        }

        return os;
    }
};
LIBTPT_NAMESPACE_END
