/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <TPT/FieldGrid.h>

#include <filesystem>
#include <memory>
#include <string_view>

LIBTPT_NAMESPACE_BEGIN
class FieldLoader {
    static constexpr std::string_view gname = "field";

public:
    ~FieldLoader();

    FieldLoader(FieldLoader const &)     = delete;
    FieldLoader(FieldLoader &&) noexcept = default;
    FieldLoader &operator=(FieldLoader const &) = delete;
    FieldLoader &operator=(FieldLoader &&) noexcept = default;

    explicit FieldLoader() = default;
    explicit FieldLoader(std::filesystem::path);

    [[nodiscard]] auto operator*() &&noexcept
    {
        return std::move(m_field);
    }

private:
    std::unique_ptr<FieldGrid> m_field;
};
LIBTPT_NAMESPACE_END
