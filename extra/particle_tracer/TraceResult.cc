/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include "TraceResult.h"

#include <PIC/TypeMaps.h>
#include <PIC/UTL/lippincott.h>

#include <HDF5Kit/HDF5Kit.h>

#include <array>
#include <chrono>
#include <functional>
#include <future>
#include <iostream>
#include <stdexcept>
#include <string>
#include <tuple>
#include <utility>

LIBTPT_NAMESPACE_BEGIN
TraceResult::~TraceResult() = default;

void TraceResult::operator<<(std::filesystem::path const path) &
{
    auto const root = [path] {
        using namespace hdf5;
        auto const file = File{ File::rdonly_tag{}, path.c_str() };
        return file.group(gname.data());
    }();

    // read attributes
    root.attribute("c").read(param.c);
    root.attribute("dt").read(param.dt);
    root.attribute("tinit").read(param.tinit);
    root.attribute("Oc0").read(param.Oc0);
    geomtr = [&root](Real xi, Real Dx, Real O0) {
        root.attribute("xi").read(xi);
        root.attribute("Dx").read(Dx);
        root.attribute("O0").read(O0);
        return Geometry{ xi, Dx, O0 };
    }({}, {}, {});
    {
        std::array<Real, 2> tlim{};
        root.attribute("tlim").read(tlim.data(), hdf5::make_type(*tlim.data()));
        if (std::get<0>(tlim) >= std::get<1>(tlim))
            throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ } + " - invalid tlim: {" + std::to_string(tlim[0]) + ", " + std::to_string(tlim[1]) + "}" };
        time_extent = { tlim.front(), tlim.back() - tlim.front() };
    }
    {
        std::array<Real, 2> q1lim{};
        root.attribute("q1lim").read(q1lim.data(), hdf5::make_type(*q1lim.data()));
        if (std::get<0>(q1lim) >= std::get<1>(q1lim))
            throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ } + " - invalid q1lim: {" + std::to_string(q1lim[0]) + ", " + std::to_string(q1lim[1]) + "}" };
        space_extent = { q1lim.front(), q1lim.back() - q1lim.front() };
    }

    // read test particle position
    first.pos = [&root] {
        auto const dset  = root.dataset("q1");
        auto       space = dset.space();
        auto const dims  = space.simple_extent().first;
        if (auto const rank = dims.rank(); rank != 1)
            throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ } + " - invalid q1 rank: " + std::to_string(rank) };

        space.select_all();
        std::vector<CurviCoord> pos(dims.front());
        static_assert(sizeof(*pos.data()) == sizeof(Real));
        dset.read(space, pos.data(), hdf5::make_type<Real>(), space);

        return pos;
    }();

    // read test particle relativistic velocity
    first.gvel = [&root] {
        auto const dset  = root.dataset("gvel");
        auto       space = dset.space();
        auto const dims  = space.simple_extent().first;
        if (auto const rank = dims.rank(); rank != 2)
            throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ } + " - invalid gvel rank: " + std::to_string(rank) };
        else if (auto const vlen = dims.back(); vlen != 3)
            throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ } + " - invalid gvel vector length: " + std::to_string(vlen) };

        space.select_all();
        std::vector<CartVector> gvel(dims.front());
        static_assert(sizeof(*gvel.data()) == 3 * sizeof(Real));
        dset.read(space, gvel.data(), hdf5::make_type<Real>(), space);

        return gvel;
    }();

    if (first.pos.size() != first.gvel.size())
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ } + " - inconsistent particle count: pos.size(" + std::to_string(first.pos.size()) + ") != gvel.size(" + std::to_string(first.gvel.size()) + ")" };
    first.bfield.resize(first.pos.size());
    first.efield.resize(first.pos.size());
}
void TraceResult::operator>>(std::filesystem::path const path) &&
{
    if (world.rank() != master)
        return;

    auto root = [path] {
        using namespace hdf5;
        auto file = File{ File::trunc_tag{}, path.c_str() };
        return file.group(gname.data(), PList::gapl(), PList::gcpl());
    }();

    // write attributes
    root.attribute("c", hdf5::make_type(param.c), hdf5::Space::scalar())
        .write(param.c);
    root.attribute("dt", hdf5::make_type(param.dt), hdf5::Space::scalar())
        .write(param.dt);
    root.attribute("tinit", hdf5::make_type(param.tinit), hdf5::Space::scalar())
        .write(param.tinit);
    root.attribute("tfinal", hdf5::make_type(param.tfinal), hdf5::Space::scalar())
        .write(param.tfinal);
    root.attribute("Oc0", hdf5::make_type(param.Oc0), hdf5::Space::scalar())
        .write(param.Oc0);
    root.attribute("xi", hdf5::make_type(geomtr.xi()), hdf5::Space::scalar())
        .write(geomtr.xi());
    root.attribute("Dx", hdf5::make_type(geomtr.D1()), hdf5::Space::scalar())
        .write(geomtr.D1());
    root.attribute("O0", hdf5::make_type(geomtr.B0()), hdf5::Space::scalar())
        .write(geomtr.B0());
    {
        std::array const tlim = { time_extent.min(), time_extent.max() };
        auto const       type = hdf5::make_type(*tlim.data());
        root.attribute("tlim", type, hdf5::Space::simple(tlim.size()))
            .write(tlim.data(), type);
    }
    {
        std::array const q1lim = { space_extent.min(), space_extent.max() };
        auto const       type  = hdf5::make_type(*q1lim.data());
        root.attribute("q1lim", type, hdf5::Space::simple(q1lim.size()))
            .write(q1lim.data(), type);
    }

    // dataset writer
    auto const write_datasets = [type = hdf5::make_type<Real>()](hdf5::Group root, Pack pack) {
        {
            auto const &pos   = pack.pos;
            auto        space = hdf5::Space::simple(pos.size());
            static_assert(sizeof(*pos.data()) == sizeof(Real));

            space.select_all();
            root.dataset("q1", type, space).write(space, pos.data(), type, space);
        }
        {
            auto const &gvel  = pack.gvel;
            auto        space = hdf5::Space::simple({ gvel.size(), 3 });
            static_assert(sizeof(*gvel.data()) == 3 * sizeof(Real));

            space.select_all();
            root.dataset("gvel", type, space).write(space, gvel.data(), type, space);
        }
        {
            auto const &bfield = pack.bfield;
            auto        space  = hdf5::Space::simple({ bfield.size(), 3 });
            static_assert(sizeof(*bfield.data()) == 3 * sizeof(Real));

            space.select_all();
            root.dataset("dB", type, space).write(space, bfield.data(), type, space);
        }
        {
            auto const &efield = pack.efield;
            auto        space  = hdf5::Space::simple({ efield.size(), 3 });
            static_assert(sizeof(*efield.data()) == 3 * sizeof(Real));

            space.select_all();
            root.dataset("dE", type, space).write(space, efield.data(), type, space);
        }
    };

    // write particles
    write_datasets(root.group("first", hdf5::PList::gapl(), hdf5::PList::gcpl()), first);
    write_datasets(root.group("last", hdf5::PList::gapl(), hdf5::PList::gcpl()), last);
}

void TraceResult::trace(ParticleTracer const &tracer, int const number_of_threads) &
try {
    auto const is_master = world.rank() == master;
    if (is_master)
        print(std::cout, "%% ", __FUNCTION__, " - tracing begin") << std::endl;

    // divide and conquer
    {
        int const mpi_rank = world.rank();
        int const mpi_size = world.size();

        auto const total_count = long(first.pos.size());
        auto const stride      = total_count / mpi_size;
        auto const index_first = mpi_rank * stride;
        auto const is_mpi_last = mpi_rank + 1 == mpi_size;
        auto const index_last  = is_mpi_last ? total_count : index_first + stride;

        first.trim(index_first, index_last);
        last = first;
    }
    auto const size = first.pos.size();

    // calculate initial fields
    for (auto i = 0UL; i < size; ++i) {
        auto const &pos    = first.pos.at(i);
        first.bfield.at(i) = tracer.bfield().interp(param.tinit, pos);
        first.efield.at(i) = tracer.efield().interp(param.tinit, pos);
    }

    // trace particles
    {
        std::vector<std::future<Real>> tasks;

        // divide and conquer
        auto const stride     = size / unsigned(number_of_threads);
        auto       index_last = 0 * stride;
        for (auto i = number_of_threads - 1; i > 0; --i) {
            auto const index_first = std::exchange(index_last, index_last + stride);
            tasks.emplace_back(
                std::async(std::launch::async, std::mem_fn(&TraceResult::impl_trace), this, false,
                           std::cref(tracer), index_first, index_last, &last));
        }
        auto const index_first = std::exchange(index_last, size);
        auto const tfinal      = impl_trace(is_master, tracer, index_first, index_last, &last);
        for (auto &future : tasks) {
            future.get();
        }
        param.tfinal = tfinal;
    }

    // calculate final fields
    for (auto i = 0UL; i < size; ++i) {
        auto const &pos   = last.pos.at(i);
        last.bfield.at(i) = tracer.bfield().interp(param.tfinal, pos);
        last.efield.at(i) = tracer.efield().interp(param.tfinal, pos);
    }

    if (is_master)
        print(std::cout, "%% ", __FUNCTION__, " - tracing end") << std::endl;
} catch (std::exception const &e) {
    fatal_error(__PRETTY_FUNCTION__, " - exception thrown:\n\t", e.what());
}
Real TraceResult::impl_trace(bool const is_master, ParticleTracer const &tracer, std::size_t const index_first, std::size_t const index_last, Pack *last) const &
{
    Real       tlast   = param.tfinal;
    auto const monitor = [&tlast](long, Real t, auto const &) noexcept {
        // we snatch time here because by the time the trace terminates,
        // the time returned will be outside the time extent.
        tlast = t;
        return true;
    };
    auto before  = std::chrono::steady_clock::now();
    auto present = before;
    for (auto i = index_first; i < index_last; ++i) {
        auto const gcgvel  = lorentz_boost<-1>(FourCartVector{ param.c, {} }, first.gvel.at(i) / param.c);
        auto const initial = RelativisticParticle{ param.Oc0, gcgvel, first.pos.at(i) };

        if (is_master) {
            using namespace std::literals::chrono_literals;
            if (present = std::chrono::steady_clock::now(); present - before > 5s) {
                before = present;
                print(std::cout, "\ttracing particle: ", initial) << std::endl;
            }
        }

        auto const final = tracer.trace(param.tinit, initial, param.dt, monitor);
        // we don't use final.first (which is the final time) because the time returned will be outside the time extent.

        last->pos.at(i)  = final.second.pos;
        last->gvel.at(i) = final.second.gcgvel.s;
    }
    return tlast;
}

template <class Comm, class T>
auto TraceResult::gather(Comm const &comm, std::vector<T> payload, int const tag)
{
    decltype(payload) gathered;

    auto ticket = comm.ibsend(std::move(payload), { master, tag });
    if (comm->rank() == master) {
        for (int rank = 0, size = comm->size(); rank < size; ++rank) {
            comm.template recv<T>({}, { rank, tag }).unpack([&gathered](auto payload) {
                gathered.insert(end(gathered), begin(payload), end(payload));
            });
        }
    }
    std::move(ticket).wait();

    return gathered;
}
void TraceResult::collect() &
{
    parallel::Communicator<CurviCoord, CartVector> const comm{ world.duplicated() };

    constexpr int tag = 39;

    first.pos    = gather(comm, std::move(first.pos), tag + 0);
    first.gvel   = gather(comm, std::move(first.gvel), tag + 1);
    first.bfield = gather(comm, std::move(first.bfield), tag + 2);
    first.efield = gather(comm, std::move(first.efield), tag + 3);

    last.pos    = gather(comm, std::move(last.pos), tag + 4);
    last.gvel   = gather(comm, std::move(last.gvel), tag + 5);
    last.bfield = gather(comm, std::move(last.bfield), tag + 6);
    last.efield = gather(comm, std::move(last.efield), tag + 7);
}
LIBTPT_NAMESPACE_END
