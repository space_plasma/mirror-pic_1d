/*
 * Copyright (c) 2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include "TraceResult.h"

#include <PIC/UTL/Options.h>

#include <filesystem>

LIBTPT_NAMESPACE_BEGIN
class Driver {
public:
    ~Driver();
    explicit Driver(parallel::mpi::Comm, Options const &);

    void operator()() &&;

private:
    int                             number_of_mpi_processes{ 1 };
    int                             number_of_threads{ 1 };
    TraceResult                     result;
    std::filesystem::path           output_path; // tracing result output path
    std::unique_ptr<ParticleTracer> tracer;

    void do_work() &;
};
LIBTPT_NAMESPACE_END
