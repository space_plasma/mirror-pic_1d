
add_executable(particle_tracer)
target_sources(particle_tracer PRIVATE
    FieldLoader.h
    FieldLoader.cc
    TraceResult.h
    TraceResult.cc
    Driver.h
    Driver.cc
    main.cc
    )

set_project_warnings(particle_tracer)
enable_sanitizers(particle_tracer)
